import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {


  constructor(private http: HttpClient, private router: Router) { }
  
  trendingMovies: any
  theatreMovies: any
  popularMovies: any

  ngOnInit(): void {
    this.getTrendingMovies()
    this.getTheatreMovies()
    this.getPopularMovies()
  }

  getTrendingMovies(){
    this.http.get('http://localhost:4200/assets/data/trending-movies.json')
    .subscribe((r)=>{this.trendingMovies=r
  })
  }

  getTheatreMovies(){
    this.http.get('http://localhost:4200/assets/data/theatre-movies.json')
    .subscribe((r)=>{this.theatreMovies=r
  })
  }

  getPopularMovies(){
    this.http.get('http://localhost:4200/assets/data/popular-movies.json')
    .subscribe((r)=>{this.popularMovies=r
      console.log(r)
  })
  }

    goToMovie(type:string, id:string) {
      this.router.navigate(['movie',type,id])
  }
}
