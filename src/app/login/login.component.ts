import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private auth: AuthService,private router:Router) { }
  username=''
  password=''
  errorMsg=''
  successMsg=''
  ngOnInit(): void {
  }

  login() {
    if(this.username.length==0){
      this.errorMsg='You must enter username'
      return
    }
    if(this.username.length<4){
      this.errorMsg='You username must be at least 4 characters'
      return
    }
    if(this.password.length==0){
      this.errorMsg='You must enter password'
      return
    }
    this.errorMsg=''  
    let res=this.auth.login(this.username,this.password)
    if (res==403){
      this.errorMsg='Wrong username or password'
    } else if (res=200) {
      this.successMsg='Success! Redirecting...'
      setTimeout(()=>{
        this.router.navigate(['home'])
      },3000)
    }
  }
  
}
