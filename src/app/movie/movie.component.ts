import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {

  constructor(private route: ActivatedRoute, private http: HttpClient) { }
  type = '';
  id = '';
  url = '';
  movie:any
  movies:any;

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id']
    this.type = this.route.snapshot.params['type']
    if (this.type === 'trending') {
      this.url = 'http://localhost:4200/assets/data/trending-movies.json';
    } else if (this.type === 'theatre') {
      this.url = 'http://localhost:4200/assets/data/theatre-movies.json'
    } else if (this.type === 'popular') {
      this.url = 'http://localhost:4200/assets/data/popular-movies.json'
    }
    this.getMovie()
  }
  
  getMovie() {
    this.http.get(this.url).subscribe((movies) => {
      this.movies=movies
      this.movie=this.movies.find((movie: { id: string; })=>movie.id==this.id)
      console.log(this.movie.reviews)
    }
    )

  }
}
